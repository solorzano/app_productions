# app
 - Running `bower_install`

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## Considerations

In the `/script/services/` file is a variable named url , which contains the 
path to where will consult `ProductionApp API` , changing the IP must pull 
in the variable url file aforementioned.

